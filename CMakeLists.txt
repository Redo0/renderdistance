
cmake_minimum_required(VERSION 3.0)

project(RenderDistance)

set(CMAKE_CXX_FLAGS "-static -static-libgcc -static-libstdc++ -Wall -Werror")

add_library(RenderDistance SHARED
	"src/RenderDistance.cpp"
	"../RedoBlHooksV20/RedoBlHooksV20.cpp"
)

target_include_directories(RenderDistance PRIVATE
	"../RedoBlHooksV20"
)

set_target_properties(RenderDistance PROPERTIES PREFIX "")

target_link_libraries(
	RenderDistance
	Psapi
)
