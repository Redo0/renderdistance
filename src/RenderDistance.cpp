
#include <Windows.h>
#include <Psapi.h>

#include "RedoBlHooksV20.hpp"

float newVisibleDistanceLimitMax = 100000.0f;

bool init(){
	BlInit;
	
	
	// Find where RenderScene loads the hardcoded visible distance limits, and make it load our numbers instead.
	ADDR BlScanHex(addrRenderSceneLoadLimit, "F3 0F 10 15 ? ? ? ? 53 8B D9 33 C9 0F 2F CA 56 57 89 0D ? ? ? ? 76 05");
	
	BlPatchInt(addrRenderSceneLoadLimit + 4, &newVisibleDistanceLimitMax);
	
	
	// Find the pointer to the frame buffer, and change it to point to a new fucking huge one
	ADDR BlScanHex(addrInitLibraries, "E8 ? ? ? ? 84 C0 75 15 68 ? ? ? ? 68 ? ? ? ? E8 ? ? ? ? 83 C4 08 32 C0 C3");
	ADDR* FrameAllocator_smBuffer_ptr = *(ADDR**)( addrInitLibraries + (0x59B315 - 0x59B2E0 + 1) );
	
	BlPrintf("Found frame allocator buffer pointer at %08x", FrameAllocator_smBuffer_ptr);
	BlPrintf("Old buffer is located at %08x", *FrameAllocator_smBuffer_ptr);
	
	*FrameAllocator_smBuffer_ptr = (ADDR)(new char[0x10000000]);
	
	BlPrintf("New buffer is located at %08x", *FrameAllocator_smBuffer_ptr);
	
	
	BlPrintf("Successfully increased render distance");
	
	return true;
}

int WINAPI DllMain(HINSTANCE hInstance, unsigned long reason, void* reserved){
	switch(reason){
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return true;
		default:
			return true;
	}
}
